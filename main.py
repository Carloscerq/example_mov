import random
import numpy as np
import matplotlib.pyplot as plt

"""
Args:
  X : 2D array of points on X-axis
  Y : 2D array of points on Y-axis
  r : goal size
  loc : goal location
Return:
  delx : potential field on X-axis
  dely : potential field on Y-axis

This function adds a goal and its potential field on the graph.
α = 50
"""


def add_goal(X, Y, s, r, loc):
    delx = np.zeros_like(X)
    dely = np.zeros_like(Y)
    for i in range(len(X)):
        for j in range(len(Y)):
            d = np.sqrt((loc[0]-X[i][j])**2 + (loc[1]-Y[i][j])**2)
            theta = np.arctan2(loc[1]-Y[i][j], loc[0]-X[i][j])
            if d < r:
                delx[i][j] = 0
                dely[i][j] = 0
            elif d > r+s:
                delx[i][j] = 50 * s * np.cos(theta)
                dely[i][j] = 50 * s * np.sin(theta)
            else:
                delx[i][j] = 50 * (d-r) * np.cos(theta)
                dely[i][j] = 50 * (d-r) * np.sin(theta)
    return delx, dely


x = np.arange(0, 50, 1)
y = np.arange(0, 50, 1)
goal = random.sample(range(0, 50), 2)
s = 7
r = 2
seek_points = np.array([[0, 0]])
X, Y = np.meshgrid(x, y)
delx, dely = add_goal(X, Y, s, r, goal)

"""
Args:
  X : 2D array of points on X-axis
  Y : 2D array of points on Y-axis
  delx : potential field on X-axis
  dely : potential field on Y-axis
  obj : string to identify if the object is Goal or Obstacle
  fig : Matplotlib figure
  ax : Axis of the figure
  loc : Location of the object
  r : Size of the object
  i : Number of the object
  color : Color of the object
  start_goal : Starting point of the robot, default = (0,0)
Returns:
  ax : Axis of the figure

This function plots the quiver plot and draws the goal/obstacle at the given location with the given color and text.
"""


def plot_graph(X, Y, delx, dely, obj, fig, ax, loc, r, i, color, start_goal=np.array([[0, 0]])):
    ax.quiver(X, Y, delx, dely)
    ax.add_patch(plt.Circle(loc, r, color=color))
    ax.set_title(f'Robot path with {i} obstacles ')
    ax.annotate(obj, xy=loc, fontsize=10, ha="center")
    return ax


"""
Args:
  X : 2D array of points on X-axis
  Y : 2D array of points on Y-axis
  delx : potential field on X-axis
  dely : potential field on Y-axis
  goal : location of the goal
Returns:
  delx : potential field on X-axis
  dely : potential field on Y-axis
  obstacle : location of the obstacle
  r : size of the obstacle

This function generates an obstacle with a diameter ranging from 1 to 5 (radius 0.5 to 2.5) randomly.
It also generates the location of the obstacle randomly.
Then, it calculates distances from each point to the goal and obstacle, and their respective angles.
The potential field is then adjusted based on the goal and obstacle locations.
α = 50
β = 120
s = 7
"""


def add_obstacle(X, Y, delx, dely, goal):
    s = 7
    r = random.randint(5, 25)/10
    obstacle = random.sample(range(0, 50), 2)

    for i in range(len(X)):
        for j in range(len(Y)):
            d_goal = np.sqrt((goal[0]-X[i][j])**2 + (goal[1]-Y[i][j])**2)
            d_obstacle = np.sqrt(
                (obstacle[0]-X[i][j])**2 + (obstacle[1]-Y[i][j])**2)

            theta_goal = np.arctan2(goal[1] - Y[i][j], goal[0] - X[i][j])
            theta_obstacle = np.arctan2(
                obstacle[1] - Y[i][j], obstacle[0] - X[i][j])

            if d_obstacle < r:
                delx[i][j] = -1 * np.sign(np.cos(theta_obstacle)) * 5 + 0
                dely[i][j] = -1 * np.sign(np.cos(theta_obstacle)) * 5 + 0
            elif d_obstacle > r+s:
                delx[i][j] += 0 - (50 * s * np.cos(theta_goal))
                dely[i][j] += 0 - (50 * s * np.sin(theta_goal))
            elif d_obstacle < r+s:
                delx[i][j] += -150 * (s+r-d_obstacle) * np.cos(theta_obstacle)
                dely[i][j] += -150 * (s+r-d_obstacle) * np.sin(theta_obstacle)

            if d_goal < r+s:
                if delx[i][j] != 0:
                    delx[i][j] += (50 * (d_goal-r) * np.cos(theta_goal))
                    dely[i][j] += (50 * (d_goal-r) * np.sin(theta_goal))
                else:
                    delx[i][j] = (50 * (d_goal-r) * np.cos(theta_goal))
                    dely[i][j] = (50 * (d_goal-r) * np.sin(theta_goal))

            if d_goal > r+s:
                if delx[i][j] != 0:
                    delx[i][j] += 50 * s * np.cos(theta_goal)
                    dely[i][j] += 50 * s * np.sin(theta_goal)
                else:
                    delx[i][j] = 50 * s * np.cos(theta_goal)
                    dely[i][j] = 50 * s * np.sin(theta_goal)

            if d_goal < r:
                delx[i][j] = 0
                dely[i][j] = 0

    return delx, dely, obstacle, r


"""
Plots 10 plots with an increasing number of obstacles. Goal is fixed at (40,40),
size of the goal is fixed to 7
"""

for i in range(3):
    fig, ax = plt.subplots(figsize=(10, 10))
    goal = [40, 40]
    delx, dely = add_goal(X, Y, s, r, goal)
    plot_graph(X, Y, delx, dely, 'Goal', fig, ax, goal, 7, 0, 'b')

    for j in range(i):
        delx, dely, loc, r = add_obstacle(X, Y, delx, dely, goal)
        plot_graph(X, Y, delx, dely, 'Obstacle', fig, ax, loc, r, j+1, 'm')

    ax.streamplot(X, Y, delx, dely, start_points=seek_points,
                  linewidth=4, cmap='autumn')
    plt.show()
